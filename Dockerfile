FROM mhart/alpine-node:8

# This image provides a Node.JS environment you can use to run your Node.JS
# applications.

MAINTAINER Ignacio Coterillo <icoteril@cern.ch>

# Create app directory
RUN adduser node -s /bin/sh -D; mkdir -p /opt/dbod/sessions; mkdir -p /opt/dbod/downloads; chown node:node /opt/dbod -R
WORKDIR /opt/dbod/

USER node
COPY CERN-CA.pem .
RUN npm install express express-request-proxy express-session jsonwebtoken session-file-store simple-oauth2@1.6.0 uniqid fs download-file elasticsearch socket.io 

ENTRYPOINT ["/bin/sh"]
